module top (
    (* LOC="C8" *)
    input clk,

    (* LOC="H11" *)
    output out
);

reg[23:0] t = 24'b0;
assign out = t[23];

always @(posedge clk) begin
    t <= t + 1;
end

endmodule
