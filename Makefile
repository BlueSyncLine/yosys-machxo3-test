all: top.bit

clean:
	rm -f top.json top.txt top.bit

top.json: top.v
	yosys -p "synth_lattice -family xo3 -json $@" $<

top.txt: top.json
	nextpnr-machxo2 --json $< --device LCMXO3LF-6900C-5BG256C --textcfg $@

top.bit: top.txt
	ecppack --compress $< $@

flash: top.bit
	openFPGALoader $<
